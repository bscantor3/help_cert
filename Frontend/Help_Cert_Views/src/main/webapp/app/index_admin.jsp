<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="modulos/componentes/navegacion_admin.jsp" />

<div class="container-fluid content-container bg-white">
    <div class="row">
        <div class="col-3 aside-menu">
            <jsp:include page="modulos/componentes/panel_admin.jsp" />
        </div>
        <div class="col-9 principal-content pl-5">
            <h5 class="text-brown-muted">Dashboard</h5>
            <hr class="w-75">
            <div class="info-items-container">
                <div class="info-item-1">
                    <div class="header-item">
                        <hr class="line-item">
                    </div>
                    <div class="content-item">
                        <h1 id="cant-certificados" class="font-weight-bold">606</h1>
                        <h4>Certificados</h4>
                    </div>
                </div>
                <div class="info-item-2">
                    <div class="header-item">
                        <hr class="line-item">
                    </div>
                    <div class="content-item">
                        <h1 id="cant-funcionarios" class="font-weight-bold">615</h1>
                        <h4>Funcionarios</h4>
                    </div>
                </div>
                <div class="info-item-3">
                    <div class="header-item">
                        <hr class="line-item">
                    </div>
                    <div class="content-item">
                        <h1 id="cant-solicitudes" class="font-weight-bold">210</h1>
                        <h4>Solicitudes</h4>
                    </div>
                </div>
            </div>
            <div class="content-bottom">
                <hr class="w-50 ">
                <img class="logo-content" src="img/icons/HC.png" alt="">
            </div>
        </div>
    </div>
</div>

<jsp:include page="modulos/componentes/pie_pagina.jsp" />
