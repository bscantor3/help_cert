<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../componentes/navegacion_${componente}.jsp" />

<div class="container-fluid content-container bg-white">
    <div class="row">
        <div class="col-3 aside-menu">
            <jsp:include page="../componentes/panel_${componente}.jsp" />
        </div>
        <div class="col-9 principal-content pl-5">
            <h5 class="text-brown-muted">Perfil -> Actualizacion De Información</h5>
            <hr class="w-75">
            <br>
            <form action="actualizarperfil" method="post">
                <div class="actualizacion-content">
                    <div class="formulario-actualizacion-content">
                        <input type="hidden" name="id" value="${usuario.idUsuario}" required>
                        <div class="edit-option-container">
                            <h5 class="text-muted">${usuario.nombres} ${usuario.apellidos}</h5>
                            <hr class="w-50">
                        </div>
                        <div class="edit-option-container">
                            <h5 class="mr-3 text-muted"> ${usuario.objTipoDocumento.abreviatura}: ${usuario.numeroDocumento}  De:</h5><input class="input-perfil" type="text" name="ciudad" placeholder="${usuario.ciudad}" value="${usuario.ciudad}" required>
                            <hr class="w-50">
                        </div>
                        <div class="edit-option-container">
                            <h5 class="mr-3 text-muted">Correo: ${usuario.correo}</h5>
                            <hr class="w-50">
                        </div>
                        <div class="edit-option-container">
                            <h5 class="mr-3 text-muted"> Teléfono: </h5><input class="input-perfil" type="text" name="telefono" placeholder="${usuario.telefono}" value="${usuario.telefono}">
                            <hr class="w-50">
                        </div>
                        <div class="edit-option-container">
                            <h5 class="mr-3 text-muted"> Dirección: </h5><input class="input-perfil" style="width:300px" type="text" name="direccion" placeholder="${usuario.direccion}" value="${usuario.direccion}" required>
                            <hr class="w-50">
                        </div>
                        <div class="edit-option-container">
                            <h5 class="mr-3 text-muted">Institución: SENA</h5>
                            <hr class="w-50">
                        </div>
                    </div> 
                    <div class="foto-perfil-content">
                        <img class="perfil-container" src="${usuario.foto}">
                        <input name="foto" type="file" required>
                    </div>
                </div>
                <div class="boton-edit-container">
                    <button class="boton" type="submit">Guardar</button>
                </div>

            </form>
            <div class="content-bottom">
                <hr class="w-50 ">
                <img class="logo-content" src="img/icons/HC.png" alt="">
            </div>
        </div>
    </div>
</div>

<jsp:include page="../componentes/pie_pagina.jsp" />
