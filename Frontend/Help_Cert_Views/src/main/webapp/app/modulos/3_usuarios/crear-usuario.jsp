<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../componentes/navegacion_contrato.jsp"/>

<div id="menu-aside" class="menu-aside-container aside-menu">
    <jsp:include page="../componentes/panel_${componente}.jsp"/>
</div>
<div class="container-fluid content-container-u bg-white">
    <div class="rutas">
        <div class="texto-rutas">
            <a class="float-left" href="home">
                <h4 class="text-brown-muted">Tabla de Información</h4>
            </a>
            <h4 class="text-brown-muted float-left px-2">-></h4>
            <a href="#">
                <h4 class="text-brown-muted float-left">Usuarios</h4>
            </a>
            <h4 class="text-brown-muted float-left px-2">-></h4>
            <a href="#">
                <h4 class="text-brown-muted float-left">Crear Usuario</h4>
            </a>
        </div>
    </div>
    <div class="formulario-crear-usuario-container">
        <form action="crearusuario" method="post">
            <div class="actualizacion-content">
                <div class="formulario-actualizacion-content">
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Rol Usuario: </h5>
                        <select name="rol" required>
                            <option value="1">Funcionario</option>
                            <option value="2">Admin de Admin</option>
                            <option value="3">Team Controller</option>
                        </select>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Nombres: </h5><input class="input-perfil" type="text" name="nombres" placeholder="Digite Nombres del Usuario" required>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Apellidos: </h5><input class="input-perfil" type="text" name="apellidos" placeholder="Digite Apellidos del Usuario" required>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Tipo de Documento: </h5>
                        <select name="tipoDocumento" required>
                            <option value="1">Cédula de Ciudadanía</option>
                            <option value="2">Tarjeta de Identidad</option>
                            <option value="3">Cédula de Extranjería</option>
                            <option value="3">Documento Nacional de Identidad</option>
                        </select>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Número Documento: </h5><input class="input-perfil" type="text" name="numeroDocumento" placeholder="Digite Número de Documento" required>
                        <hr class="w-50">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Género: </h5>
                        <select name="genero" required>
                            <option value="1">Masculino</option>
                            <option value="2">Femenino</option>
                            <option value="3">Otro</option>
                        </select>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Ciudad: </h5><input class="input-perfil" type="text" name="ciudad" placeholder="Digite Ciudad" required>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Correo: </h5><input class="input-perfil" type="email" name="correo" placeholder="Digite Correo Electrónico" required>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Dirección: </h5><input class="input-perfil" type="text" name="direccion" placeholder="Digite Dirección" required>
                        <hr class="w-75">
                    </div>
                    <div class="edit-option-container">
                        <h5 class="mr-3 text-muted"> Teléfono: </h5><input class="input-perfil" type="text" name="telefono" placeholder="Digite Teléfono" required>
                        <hr class="w-75">
                    </div>
                </div> 
                <div class="foto-perfil-content">
                    <img class="perfil-container" src="">
                    <input name="foto" type="file" required>
                </div>
            </div>
            <div class="boton-edit-container">
                <button class="boton" type="submit">Guardar</button>
            </div>

        </form>
    </div>
</div>

<jsp:include page="../componentes/pie_pagina.jsp"/>