<%@page contentType="text/html" pageEncoding="UTF-8"%>

<footer class="container-fluid footer-container">
    <img class="icon-footer" src="img/icons/icon-footer.png" alt="">
    <h2 class="text-white mt-4">HELP CERT</h2>
    <hr class="hr-footer">
    <p class="text-white mt-2">© Copyright 2020 - Todos los Derechos Reservados</p>
</footer>
<script src="js/dashboard.js"></script>
<script src="js/menu.js"></script>


</body>

</html>