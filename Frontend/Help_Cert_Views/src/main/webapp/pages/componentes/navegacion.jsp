<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Help_Cert</title>
    <link rel="stylesheet" href="css/home.css">
    <link rel="shortcut icon" href="img/icons/HC.png" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Segoe+UI' rel='stylesheet'>
</head>

<body>
    <div class="container">
        <nav class="nav-container">
            <a href="homepage"><img src="img/logos/sena-logo.png" alt=""></a>
            <div class="navegacion-container">
                <a href="sitepages?action=inicio"><img src="img/icons/home-icon.png"> INICIO</a>
                <a href="sitepages?action=acercade"><img class="item-nav" src="img/icons/acerca-icon.png"> ACERDA DE</a>
                <a href="sitepages?action=servicios"><img class="item-nav" src="img/icons/servicios-icon.png"> SERVICIOS</a>
                <a href="sitepages?action=contacto"><img class="item-nav" src="img/icons/contacto-icon.png"> CONTACTO</a>
            </div>
        </nav>