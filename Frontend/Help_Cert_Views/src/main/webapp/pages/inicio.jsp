<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="componentes/navegacion.jsp" />

<section class="home-section-1">
    <div class="login-container">
        <div class="info-login">
            <img class="foto" src="img/icons/user-icon.png" alt="">
            <form action="home" method="post">
                <input id="email" class="email" type="email" name="correo" placeholder="email">
                <input class="clave" type="password" name="clave" placeholder="password">
                <a class="olvidaste" href="">¿Olvidaste tu Contraseña?</a>
                <button type="submit" id="login" class="boton1">Login</button>
            </form>
        </div>
    </div>
</section>


<jsp:include page="componentes/pie-de-pagina.jsp" />