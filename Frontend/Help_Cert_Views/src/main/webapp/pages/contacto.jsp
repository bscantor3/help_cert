<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="componentes/navegacion.jsp" />

<section class="home-section-5">
    <img class="contactanos" src="img/others/contacto-header.png" alt="">
    <div class="contactanos-container">
        <form class="datos" action="">
            <input class="info-form" type="text" placeholder="Informacion general">
            <input class="info-form1" type="text" placeholder="Nombre completo">
            <input class="info-form2" type="text" placeholder="Telefono">
            <input class="info-form3" type="email" placeholder="Email">
            <input class="info-form4" type="text" placeholder="Sede">
            <textarea class="info-form5" type="text" placeholder="Mensaje"></textarea>
        </form>
    </div>
    <a class="boton">ENVIAR</a>
</section>

<jsp:include page="componentes/pie-de-pagina.jsp" />