/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logics;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import models.Genero;
import models.RolUsuario;
import models.TipoDocumento;
import models.Usuario;

/**
 *
 * @author genyu
 */
public class UsuarioDao {

    DbConnection conn;

    public UsuarioDao(DbConnection conn) {
        this.conn = conn;
    }

    public Usuario login(String correo, String clave) throws IOException {

        try {

            String sql = "SELECT * FROM usuarios "
                    + "INNER JOIN tipo_documentos ON usuarios.fk_tipo_documento = tipo_documentos.id_tipo_documento "
                    + "INNER JOIN generos ON usuarios.fk_genero = generos.id_genero "
                    + "INNER JOIN rol_usuarios ON usuarios.fk_rol_usuario = rol_usuarios.id_rol_usuario "
                    + "WHERE correo=? AND clave=md5(?) AND estado='activo' limit 1";
            

            PreparedStatement ps = conn.getConnection().prepareStatement(sql);

            ps.setString(1, correo);

            ps.setString(2, clave);
            ResultSet rs = ps.executeQuery(); //Consulta con toda la info
            //id , nombre, estatus, username, email, pass; 
            Usuario usuario = new Usuario(); //Creamos un espacio en memoria
            
           

            while (rs.next()) {
                usuario.setIdUsuario(rs.getInt("id_usuario"));
                
                 System.out.println(rs.getInt("id_usuario"));

                usuario.setObjGenero(new Genero(
                        rs.getInt("id_genero"),
                        rs.getString("nombre_genero"),
                        rs.getString("abreviatura")));

                usuario.setObjTipoDocumento(new TipoDocumento(
                        rs.getInt("id_tipo_documento"),
                        rs.getString("nombre_documento"),
                        rs.getString("abreviatura")));

                usuario.setObjRolUsuario(new RolUsuario(
                        rs.getInt("id_rol_usuario"),
                        rs.getString("nombre_rol")));

                usuario.setNombres(rs.getString("nombres"));
                usuario.setApellidos(rs.getString("apellidos"));
                usuario.setNumeroDocumento(rs.getInt("numero_documento"));
                usuario.setCorreo(rs.getString("correo"));
                usuario.setClave(rs.getString("clave"));
                usuario.setDireccion(rs.getString("direccion"));
                usuario.setTelefono(rs.getString("telefono"));
                //usuario.setFotoBytes(rs.getBytes("foto"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setCiudad(rs.getString("ciudad"));

                /*try {
                    byte[] bi = usuario.getFotoBytes();
                    BufferedImage image = null;
                    InputStream in = new ByteArrayInputStream(bi);
                    image = ImageIO.read(in);

                    ImageIO.write(image, "png", new File("fotoPerfil.png"));
                    byte[] imageBytes = Files.readAllBytes(Paths.get("fotoPerfil.png"));

                    Base64.Encoder encoder = Base64.getEncoder();
                    String encoding = "data:image/png;base64," + encoder.encodeToString(imageBytes);

                    usuario.setFoto(encoding);

                } catch (Exception ex) {
                    System.out.println("Error al desencriptar imagen");
                }*/

            }

            return usuario;

        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.login: " + e.getMessage());
            return null;
        }

    }

    public boolean insert(Usuario usuario) {
        try {

            String sql = "INSERT INTO usuarios (fk_genero,fk_tipo_documento,fk_rol_usuario,nombres,apellidos,numero_documento,ciudad,correo,clave,direccion,telefono,foto,estado,created_at,updated_at) "
                    + "VALUES (?,?,?,?,?,?,?,?,md5(?),?,?,?,?,?,?)";

            PreparedStatement ps = conn.getConnection().prepareStatement(sql);

            ps.setInt(1, usuario.getObjGenero().getIdGenero());
            ps.setInt(2, usuario.getObjTipoDocumento().getIdTipoDocumento());
            ps.setInt(3, usuario.getObjRolUsuario().getIdRolUsuario());
            ps.setString(4, usuario.getNombres());
            ps.setString(5, usuario.getApellidos());
            ps.setInt(6, usuario.getNumeroDocumento());
            ps.setString(7, usuario.getCiudad());
            ps.setString(8, usuario.getCorreo());
            ps.setString(9, "C4S10");
            ps.setString(10, usuario.getDireccion());
            ps.setString(11, usuario.getTelefono());
            ps.setBytes(12, usuario.getFotoBytes());
            ps.setString(13, "activo");
            ps.setTimestamp(14, usuario.getCreatedAt());
            ps.setTimestamp(15, usuario.getUpdatedAt());

            ps.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.insert" + e.getMessage());
            return false;
        }
    }

    public Usuario getById(int idUsuario) {
        try {
            String sql = "SELECT * FROM usuarios "
                    + "INNER JOIN tipo_documentos ON usuarios.fk_tipo_documento = tipo_documentos.id_tipo_documento "
                    + "INNER JOIN generos ON usuarios.fk_genero = generos.id_genero "
                    + "INNER JOIN rol_usuarios ON usuarios.fk_rol_usuario = rol_usuarios.id_rol_usuario "
                    + "WHERE idUsuario=? limit 1";
            PreparedStatement ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, idUsuario);
            ResultSet rs = ps.executeQuery();
            Usuario usuario = new Usuario();

            while (rs.next()) {

                usuario.setIdUsuario(rs.getInt("id_usuario"));

                usuario.setObjGenero(new Genero(
                        rs.getInt("id_genero"),
                        rs.getString("nombre_genero"),
                        rs.getString("abreviatura")));

                usuario.setObjTipoDocumento(new TipoDocumento(
                        rs.getInt("id_tipo_documento"),
                        rs.getString("nombre_documento"),
                        rs.getString("abreviatura")));

                usuario.setObjRolUsuario(new RolUsuario(
                        rs.getInt("id_rol_usuario"),
                        rs.getString("nombre_rol")));

                usuario.setNombres(rs.getString("nombres"));
                usuario.setApellidos(rs.getString("apellidos"));
                usuario.setNumeroDocumento(rs.getInt("numero_documento"));
                usuario.setCorreo(rs.getString("correo"));
                usuario.setClave(rs.getString("clave"));
                usuario.setDireccion(rs.getString("direccion"));
                usuario.setTelefono(rs.getString("telefono"));
                usuario.setFotoBytes(rs.getBytes("foto"));
                usuario.setEstado(rs.getString("estado"));
                usuario.setCiudad(rs.getString("ciudad"));

                try {
                    byte[] bi = usuario.getFotoBytes();
                    BufferedImage image = null;
                    InputStream in = new ByteArrayInputStream(bi);
                    image = ImageIO.read(in);

                    ImageIO.write(image, "png", new File("fotoPerfil.png"));
                    byte[] imageBytes = Files.readAllBytes(Paths.get("fotoPerfil.png"));

                    Base64.Encoder encoder = Base64.getEncoder();
                    String encoding = "data:image/png;base64," + encoder.encodeToString(imageBytes);

                    usuario.setFoto(encoding);

                } catch (Exception ex) {
                    System.out.println("Error al desencriptar imagen");
                }

                usuario.setCreatedAt(rs.getTimestamp("createdAt"));
                usuario.setUpdatedAt(rs.getTimestamp("updatedAt"));

            }
            return usuario;

        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.getById: " + e.getMessage());
            return null;
        }
    }

    public List<Usuario> getAll(int idUsuario) throws IOException {
        try {

            String sql = "SELECT * FROM usuarios "
                    + "INNER JOIN tipo_documentos ON usuarios.fk_tipo_documento = tipo_documentos.id_tipo_documento "
                    + "INNER JOIN generos ON usuarios.fk_genero = generos.id_genero "
                    + "INNER JOIN rol_usuarios ON usuarios.fk_rol_usuario = rol_usuarios.id_rol_usuario "
                    + "WHERE usuarios.id_usuario <> ? ORDER BY id_usuario DESC";

            PreparedStatement ps = conn.getConnection().prepareStatement(sql);

            ps.setInt(1, idUsuario);
            
            ResultSet rs = ps.executeQuery();

            List<Usuario> list = new LinkedList<>();
            Usuario user;

            while (rs.next()) {

                user = new Usuario();

                user.setIdUsuario(rs.getInt("id_usuario"));

                user.setObjGenero(new Genero(
                        rs.getInt("id_genero"),
                        rs.getString("nombre_genero"),
                        rs.getString("abreviatura")));

                user.setObjTipoDocumento(new TipoDocumento(
                        rs.getInt("id_tipo_documento"),
                        rs.getString("nombre_documento"),
                        rs.getString("abreviatura")));

                user.setObjRolUsuario(new RolUsuario(
                        rs.getInt("id_rol_usuario"),
                        rs.getString("nombre_rol")));

                user.setNombres(rs.getString("nombres"));
                user.setApellidos(rs.getString("apellidos"));
                user.setNumeroDocumento(rs.getInt("numero_documento"));
                user.setCorreo(rs.getString("correo"));
                user.setDireccion(rs.getString("direccion"));
                user.setTelefono(rs.getString("telefono"));
                user.setCiudad(rs.getString("ciudad"));

                list.add(user);
            }
            
            return list;

        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.getall" + e.getMessage());
            return null;
        }
    }

    public boolean update(Usuario usuario) {
        return false;
    }

    public int delete(int idUsuario) {
        try {
            String sql = "DELETE FROM usuarios WHERE id_usuario=?";
            PreparedStatement ps = conn.getConnection().prepareStatement(sql);
            ps.setInt(1, idUsuario);
            int rows = ps.executeUpdate();
            return rows;
        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.eliminar: " + e.getMessage());
            return 0;
        }
    }

    public boolean updatePerfil(Usuario usuario) {
        String sql = "UPDATE usuarios SET ciudad=?, telefono=?, direccion=? WHERE id_usuario=?";
        try {

            PreparedStatement ps = conn.getConnection().prepareStatement(sql);

            ps.setString(1, usuario.getCiudad());
            ps.setString(2, usuario.getTelefono());
            ps.setString(3, usuario.getDireccion());
            ps.setInt(4, usuario.getIdUsuario());

            ps.executeUpdate();
            return true;

        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.updatePerfil : " + e.getMessage());
            return false;
        }
    }

    public boolean updateClave(Usuario usuario, String claveReg) {
        String sql = "UPDATE usuarios SET clave=md5(?) WHERE clave=md5(?) AND id_usuario=?";
        try {

            PreparedStatement ps = conn.getConnection().prepareStatement(sql);

            ps.setString(1, usuario.getClave());
            ps.setString(2, claveReg);
            ps.setInt(3, usuario.getIdUsuario());

            int i = ps.executeUpdate();

            return i == 1;

        } catch (SQLException e) {
            System.out.println("Error UsuarioDao.updateClave : " + e.getMessage());
            return false;
        }
    }

}
