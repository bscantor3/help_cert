/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.InputStream;
import java.sql.Timestamp;

/**
 *
 * @author genyu
 */
public class Usuario {
    
    private int idUsuario;
    private Genero objGenero;
    private TipoDocumento objTipoDocumento;
    private RolUsuario objRolUsuario;
    private String nombres;
    private String apellidos;
    private int numeroDocumento;
    private String ciudad;
    private String correo;
    private String clave;
    private String direccion;
    private String telefono;
    private byte[] fotoBytes;
    private String foto;
   
    private InputStream archivo;
    private String estado;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    public Usuario() {
        
    }

    public Usuario(int idUsuario, Genero objGenero, TipoDocumento objTipoDocumento, RolUsuario objRolUsuario, String nombres, String apellidos, int numeroDocumento, String ciudad, String correo, String clave, String direccion, String telefono, byte[] fotoBytes, String estado) {
        this.idUsuario = idUsuario;
        this.objGenero = objGenero;
        this.objTipoDocumento = objTipoDocumento;
        this.objRolUsuario = objRolUsuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.numeroDocumento = numeroDocumento;
        this.ciudad = ciudad;
        this.correo = correo;
        this.clave = clave;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fotoBytes = fotoBytes;
        this.estado = estado;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Genero getObjGenero() {
        return objGenero;
    }

    public void setObjGenero(Genero objGenero) {
        this.objGenero = objGenero;
    }

    public TipoDocumento getObjTipoDocumento() {
        return objTipoDocumento;
    }

    public void setObjTipoDocumento(TipoDocumento objTipoDocumento) {
        this.objTipoDocumento = objTipoDocumento;
    }

    public RolUsuario getObjRolUsuario() {
        return objRolUsuario;
    }

    public void setObjRolUsuario(RolUsuario objRolUsuario) {
        this.objRolUsuario = objRolUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public byte[] getFotoBytes() {
        return fotoBytes;
    }

    public void setFotoBytes(byte[] fotoBytes) {
        this.fotoBytes = fotoBytes;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public InputStream getArchivo() {
        return archivo;
    }

    public void setArchivo(InputStream archivo) {
        this.archivo = archivo;
    }
    
    

    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", objGenero=" + objGenero + ", objTipoDocumento=" + objTipoDocumento + ", objRolUsuario=" + objRolUsuario + ", nombres=" + nombres + ", apellidos=" + apellidos + ", numeroDocumento=" + numeroDocumento + ", correo=" + correo + ", clave=" + clave + ", direccion=" + direccion + ", telefono=" + telefono + ", fotoBytes=" + fotoBytes + ", foto=" + foto + ", estado=" + estado + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + '}';
    }

    
}
