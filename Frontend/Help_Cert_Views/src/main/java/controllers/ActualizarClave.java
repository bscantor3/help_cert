/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logics.DbConnection;
import logics.UsuarioDao;
import models.Usuario;

/**
 *
 * @author genyu
 */
public class ActualizarClave extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        RequestDispatcher rd;

        int idParam = Integer.parseInt(request.getParameter("id"));
        String claveRegParam = request.getParameter("claveReg");
        String claveNueParam = request.getParameter("claveNue");

        HttpSession session = request.getSession();
        Usuario user = new Usuario();
        user = (Usuario) session.getAttribute("usuario");

        System.out.println(user.getClave());
        System.out.println(claveRegParam);

        Usuario usuario = new Usuario();
        usuario.setIdUsuario(idParam);
        usuario.setClave(claveNueParam);

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        boolean status = usuarioDao.updateClave(usuario, claveRegParam);

        conn.disconnect();

        if (status == true) {
            LogoutController objLog = new LogoutController();
            objLog.doGet(request, response);
        } else {

            switch (user.getObjRolUsuario().getNombreRol()) {

                case "Funcionario":
                    request.setAttribute("componente", "funcionario");
                    break;

                case "Admin de Admin":
                    request.setAttribute("componente", "admin");
                    break;

                case "Team Controller":
                    request.setAttribute("componente", "team");
                    break;
            }

            rd = request.getRequestDispatcher("app/modulos/1_perfil/cambio_clave.jsp");
            rd.forward(request, response);
        }

    }
}
