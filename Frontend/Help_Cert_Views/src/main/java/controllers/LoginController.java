/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logics.DbConnection;
import logics.UsuarioDao;
import models.Usuario;

/**
 *
 * @author genyu
 */
public class LoginController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        Usuario usuario = new Usuario();
        usuario = (Usuario) session.getAttribute("usuario");

        RequestDispatcher rd;

        if (session.getAttribute("usuario") == null) {
            response.sendRedirect(request.getContextPath() + "/homepage");
        } else {

            switch (usuario.getObjRolUsuario().getNombreRol()) {
                case "Funcionario":
                    rd = request.getRequestDispatcher("app/index_funcionario.jsp");
                    rd.forward(request, response);
                    break;

                case "Admin de Admin":
                    rd = request.getRequestDispatcher("app/index_admin.jsp");
                    rd.forward(request, response);
                    break;

                case "Team Controller":
                    rd = request.getRequestDispatcher("app/index_team.jsp");
                    rd.forward(request, response);
                    break;
            }

        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        String userParam = request.getParameter("correo");
        String passParam = request.getParameter("clave");
        String msg = "";

        System.out.println(userParam + passParam);

        HttpSession session = request.getSession();

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        
        

        Usuario usuario = usuarioDao.login(userParam, passParam);
        
        System.out.println(usuario.getApellidos());
        conn.disconnect();

        RequestDispatcher rd;

        if (usuario.getIdUsuario() > 0) {
            session.setAttribute("usuario", usuario);

            switch (usuario.getObjRolUsuario().getNombreRol()) {

                case "Funcionario":
                    rd = request.getRequestDispatcher("app/index_funcionario.jsp");
                    rd.forward(request, response);
                    break;

                case "Admin de Admin":
                    rd = request.getRequestDispatcher("app/index_admin.jsp");
                    rd.forward(request, response);
                    break;

                case "Team Controller":
                    rd = request.getRequestDispatcher("app/index_team.jsp");
                    rd.forward(request, response);
                    break;
            }

        } else {
            rd = request.getRequestDispatcher("pages/inicio.jsp");
            rd.forward(request, response);
        }

    }

}
