/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import logics.DbConnection;
import logics.UsuarioDao;
import models.Usuario;

/**
 *
 * @author genyu
 */
public class ActualizarPerfilController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        int idParam = Integer.parseInt(request.getParameter("id"));
        String ciudadParam = request.getParameter("ciudad");
        String telefonoParam = request.getParameter("telefono");
        String direccionParam = request.getParameter("direccion");

        Usuario usuario = new Usuario();
        usuario.setIdUsuario(idParam);
        usuario.setCiudad(ciudadParam);
        usuario.setTelefono(telefonoParam);
        usuario.setDireccion(direccionParam);

        /*InputStream inputStream = null;
        Part filePart = request.getPart("foto");
        System.out.println(filePart);
        inputStream = filePart.getInputStream();
       
        usuario.setArchivo(inputStream);*/

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        boolean status = usuarioDao.updatePerfil(usuario);

        conn.disconnect();

        RequestDispatcher rd;

        LogoutController objLog = new LogoutController();
        objLog.doGet(request, response);

    }
}
