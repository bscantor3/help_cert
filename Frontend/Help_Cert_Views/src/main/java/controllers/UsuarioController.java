/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import logics.DbConnection;
import logics.UsuarioDao;
import models.Usuario;

/**
 *
 * @author genyu
 */
public class UsuarioController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        Usuario usuario = new Usuario();
        usuario = (Usuario) session.getAttribute("usuario");

        switch (usuario.getObjRolUsuario().getNombreRol()) {

            case "Funcionario":
                request.setAttribute("componente", "funcionario");
                break;

            case "Admin de Admin":
                request.setAttribute("componente", "admin");
                break;

            case "Team Controller":
                request.setAttribute("componente", "team");
                break;
        }

        RequestDispatcher rd;

        switch (action) {

            case "crearusuario":

                if (usuario.getObjRolUsuario().getNombreRol().equals("Funcionario")) {
                    session.invalidate();
                    response.sendRedirect(request.getContextPath() + "/homepage");

                } else {
                    rd = request.getRequestDispatcher("app/modulos/3_usuarios/crear-usuario.jsp");
                    rd.forward(request, response);
                }

                break;

            case "consultarusuario":

                if (usuario.getObjRolUsuario().getNombreRol().equals("Funcionario")) {
                    session.invalidate();
                    response.sendRedirect(request.getContextPath() + "/homepage");

                } else {
                    this.verTodas(request, response);
                }

                break;

            case "actualizarusuario":
                if (usuario.getObjRolUsuario().getNombreRol().equals("Funcionario")) {
                    session.invalidate();
                    response.sendRedirect(request.getContextPath() + "/homepage");

                } else {
                    this.actualizarUsuario(request, response);
                }

                break;

            case "eliminarusuario":
                if (usuario.getObjRolUsuario().getNombreRol().equals("Funcionario")) {
                    session.invalidate();
                    response.sendRedirect(request.getContextPath() + "/homepage");

                } else {
                    System.out.println("pruebatest");
                    this.eliminarUsuario(request, response);
                }

                break;

        }

    }

    protected void verTodas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        
        HttpSession session = request.getSession();
        Usuario usuario = new Usuario();
        usuario = (Usuario) session.getAttribute("usuario");

        List<Usuario> lista = usuarioDao.getAll(usuario.getIdUsuario());
        conn.disconnect();

        request.setAttribute("users", lista);
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("app/modulos/3_usuarios/consultar-usuario.jsp");
        rd.forward(request, response);

    }

    protected void actualizarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int idUsuario = Integer.parseInt(request.getParameter("id"));

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        Usuario usuario = usuarioDao.getById(idUsuario);

        conn.disconnect();

        request.setAttribute("usuario", usuario);
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("app/modulos/2_vacantes/actualizar-usuario.jsp");
        rd.forward(request, response);

    }

    protected void eliminarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int idUsuario = Integer.parseInt(request.getParameter("id"));

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        int respuesta = usuarioDao.delete(idUsuario);

        conn.disconnect();

        RequestDispatcher rd;

        rd = request.getRequestDispatcher("app/modulos/3_usuarios/consultar-usuario.jsp");
        rd.forward(request, response);

    }

}
