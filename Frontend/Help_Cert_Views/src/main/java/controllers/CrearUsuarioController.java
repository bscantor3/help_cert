/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import logics.DbConnection;
import logics.UsuarioDao;
import models.Genero;
import models.RolUsuario;
import models.TipoDocumento;
import models.Usuario;

/**
 *
 * @author genyu
 */
public class CrearUsuarioController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        String nombresParam = request.getParameter("nombres");
        String apellidosParam = request.getParameter("apellidos");
        int tipoDocumentoParam = Integer.parseInt(request.getParameter("tipoDocumento"));
        int numeroDocumentoParam = Integer.parseInt(request.getParameter("numeroDocumento"));
        int generoParam = Integer.parseInt(request.getParameter("genero"));
        int rolParam = Integer.parseInt(request.getParameter("rol"));
        String ciudadParam = request.getParameter("ciudad");
        String correoParam = request.getParameter("correo");
        String direccionParam = request.getParameter("direccion");
        String telefonoParam = request.getParameter("telefono");

        Usuario usuario = new Usuario();
        usuario.setNombres(nombresParam);
        usuario.setApellidos(apellidosParam);
        usuario.setObjTipoDocumento(new TipoDocumento());
        usuario.getObjTipoDocumento().setIdTipoDocumento(tipoDocumentoParam);
        usuario.setObjGenero(new Genero());
        usuario.getObjGenero().setIdGenero(generoParam);
        usuario.setObjRolUsuario(new RolUsuario());
        usuario.getObjRolUsuario().setIdRolUsuario(rolParam);
        usuario.setNumeroDocumento(numeroDocumentoParam);
        usuario.setCiudad(ciudadParam);
        usuario.setCorreo(correoParam);
        usuario.setDireccion(direccionParam);
        usuario.setTelefono(telefonoParam);

        DbConnection conn = new DbConnection();
        UsuarioDao usuarioDao = new UsuarioDao(conn);
        boolean status = usuarioDao.insert(usuario);

        conn.disconnect();

        RequestDispatcher rd;

        if (status) {
            System.out.println("prueva");
            response.sendRedirect(request.getContextPath() + "/usuarios?action=consultarusuario");
        } else {
            System.out.println("prueva");
            response.sendRedirect(request.getContextPath() + "/usuarios?action=consultarusuario");
        }
        
    }
}
