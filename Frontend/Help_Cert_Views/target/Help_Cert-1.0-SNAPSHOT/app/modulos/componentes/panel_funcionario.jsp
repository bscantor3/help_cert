<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="component-aside-1">
    <h5 class="text-brown-muted font-weight-bold ">Certificaciones</h5>
    <hr class="m-0 mb-3">
    <div class="item-component-aside">
        <img class="icon-aside-menu-2 ml-3" src="img/icons/icon-menu-6.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Por Funciones</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
    <div class="item-component-aside">
        <img class="icon-aside-menu-2 ml-3" src="img/icons/icon-menu-7.png" alt="">
        <a class="" href="../../index.html">
            <p class="text-muted mt-3 item-link-aside">Laborales</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
</div>
<div class="component-aside-2">
    <h5 class="text-brown-muted font-weight-bold ">Contratos</h5>
    <hr class="m-0 mb-3">
    <div class="item-component-aside">
        <img class="icon-aside-menu-2 ml-3" src="img/icons/icon-menu-6.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Actual Contrato</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
</div>
<div class="component-aside-3">
    <h5 class="text-brown-muted font-weight-bold ">Solicitudes</h5>
    <hr class="m-0 mb-3">
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-3" src="img/icons/icon-menu-3.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Histórico Solicitudes</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-3" src="img/icons/icon-menu-4.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Creación Novedad</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
</div>
