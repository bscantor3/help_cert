<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Help_Cert</title>
        <link rel="shortcut icon" href="app/img/icons/HC.png" type="image/x-icon">
        <link href='https://fonts.googleapis.com/css?family=Segoe+UI' rel='stylesheet'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="app/css/main.css">
        <link rel="stylesheet" href="app/css/scan.css">
    </head>

    <body>
        <nav class="navbar navbar-expand bg-white max-height-box shadow-box p-0">
            <div class="navbar-header principal-menu shadow-box">
                <div class="navbar-brand" href="#">
                    <button id="button-menu" class="ml-3 button-menu-cert">
                        <img class="ml-1 menu-icon" src="app/img/icons/menu-icon.png" alt="">
                    </button>
                    <h4 class="text-brown font-weight-bold float-right ml-3 mt-2">Team Controller</h4>
                </div>
            </div>
            <div class="bg-orange principal-nav">
                <img class="transition-icon" src="img/icons/transition-icon.png" alt="">
                <ul class="navbar-nav items-principal-nav-container">
                    <li class="nav-item item-principal-nav">
                        <a class="notifications-item" href="home">
                            <img class="notifications-icon mr-4" src="img/icons/notifications-icon.png" alt="">
                        </a>
                        <hr class="vertical-line">
                    </li>
                    <li class="nav-item item-principal-nav">
                        <a class="perfil-item" href="perfil?action=actualizar">
                            <img class="notifications-icon mx-2 d-inline-block fotoPerfil" src="${usuario.foto}" alt="">
                            <h6 class="float-right mx-2 pt-2 text-white font-weight-bold">${usuario.nombres} ${usuario.apellidos} </h6>
                        </a>
                    </li>
                    <li class="nav-item item-principal-nav">
                        <hr class="vertical-line">
                        <div class="container-options-item">
                            <button class="options-item" id="options-button">
                                <img class="notifications-icon mr-4" src="img/icons/options-icon.png" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
                <div id="options" class="options-container bg-white">
                    <div class="item-component-options">
                        <img class="icon-options-menu ml-2" src="img/icons/icon-actualizar.png" alt="">
                        <a class="text-component-options" href="perfil?action=actualizar">
                            <p class="text-muted mt-3 item-link-aside">Actualizar Información</p>
                            <hr class="m-0 mb-2">
                        </a>
                    </div>
                    <div class="item-component-options">
                        <img class="icon-options-menu ml-2" src="img/icons/icon-password.png" alt="">
                        <a class="text-component-options" href="perfil?action=clave">
                            <p class="text-muted mt-3 item-link-aside">Cambio Contraseña</p>
                            <hr class="m-0 mb-2">
                        </a>
                    </div>
                    <div class="item-component-options">
                        <img class="icon-options-menu ml-2" src="img/icons/icon-sesion.png" alt="">
                        <a class="text-component-options" href="salir">
                            <p class="text-muted mt-3 item-link-aside">Cerrar Sesión</p>
                            <hr class="m-0 mb-2">
                        </a>
                    </div>
                </div>
            </div>
        </nav>
