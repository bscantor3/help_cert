<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="component-aside-1">
    <h5 class="text-brown-muted font-weight-bold ">Team Controller</h5>
    <hr class="m-0 mb-3">
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-2" src="img/icons/icon-menu-1.png" alt="">
        <a class="" href="usuarios?action=consultarusuario">
            <p class="text-muted mt-3 item-link-aside">Tabla de Información</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-2" src="img/icons/icon-menu-2.png" alt="">
        <a class="" href="usuarios?action=crearusuario">
            <p class="text-muted mt-3 item-link-aside">Crear Usuario</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
</div>
<div class="component-aside-2">
    <h5 class="text-brown-muted font-weight-bold ">Funcionarios</h5>
    <hr class="m-0 mb-3">
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-2" src="img/icons/icon-menu-1.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Tabla de Información</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
</div>
<div class="component-aside-3">
    <h5 class="text-brown-muted font-weight-bold ">Solicitudes</h5>
    <hr class="m-0 mb-3">
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-3" src="img/icons/icon-menu-3.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Registro de Novedades</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-3" src="img/icons/icon-menu-4.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Asignación Controller</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
    <div class="item-component-aside">
        <img class="icon-aside-menu ml-3" src="img/icons/icon-menu-5.png" alt="">
        <a class="" href="">
            <p class="text-muted mt-3 item-link-aside">Tiempos de Espera</p>
            <hr class="m-0 mb-2">
        </a>
    </div>
</div>
