<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="componentes/navegacion.jsp" />

<section class="home-section-3">
    <img class="logo.acerca_de" src="img/others/acercaDe-header.png" alt="">
    <div class="mensaje">
        <h3 class="titulo">OFRECEMOS LA MEJOR CALIDAD Y SERVICIOS</h3>
    </div>
    <div class="cuadros">
        <div class="item-vision">
            <div class="title-item">
                <h3>VISION</h3>
            </div>
            <div class="paragraph-item">
                <p>Ser la Empresa preferida de Comercios
                    y Entidades para el desarrollo
                    de sistemas y aplicaciones en Colombia.</p>
            </div>
        </div>
        <div class="item-mision">
            <div class="title-item">
                <h3>MISION</h3>
            </div>
            <div class="paragraph-item">
                <p>Servir como solución del
                    desarrollo de la
                    industria de sistemas informáticos.</p>
            </div>
        </div>
        <div class="item-objetivo">
            <div class="title-item">
                <h3>OBJETIO</h3>
            </div>
            <div class="paragraph-item">
                <p>Diseñar un sistema de información que facilite y optimice los procesos de certificaciones laborales
                    de CEET.con cualidades de flexibilidad que permitan a futuro la implementación de en otros procesos
                </p>
            </div>
        </div>
    </div>
</section>

<jsp:include page="componentes/pie-de-pagina.jsp" />