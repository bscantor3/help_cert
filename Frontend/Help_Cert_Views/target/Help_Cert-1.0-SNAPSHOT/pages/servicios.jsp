<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="componentes/navegacion.jsp" />

<section class="home-section-4">
    <img class="servicios" src="img/others/servicios-header.png" alt="">
    <div class="text-servicios">
        <div class="cuadro">
            <p>Trabajamos en desarrollo de aplicaciones a medida de las necesidades de nuestros clientes.
                Seguimos en todos los casos una metodología de desarrollo, aplicando los procedimientos y
                controles de nuestro sistema de gestión de la calidad </p>
        </div>
    </div>
    <div class="logo-container2">
        <img class="logo.hc" src="img/logos/logo-HC.png" alt="">
    </div>
</section>
<jsp:include page="componentes/pie-de-pagina.jsp" />